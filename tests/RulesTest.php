<?php

class RulesTest extends PHPUnit_Framework_TestCase {

  /**
   * @var Cell
   */
  protected $object;

  protected function setUp() {
    $this->object = new \Rules();
  }

  /**
   * @test
   */
  public function rules() {
    $this->assertFalse($this->object->getNextState(0, true));
    $this->assertFalse($this->object->getNextState(1, true));
    $this->assertTrue($this->object->getNextState(2, true));
    $this->assertTrue($this->object->getNextState(3, true));
    $this->assertFalse($this->object->getNextState(4, true));
    $this->assertFalse($this->object->getNextState(2, false));
    $this->assertTrue($this->object->getNextState(3, false));
    $this->assertFalse($this->object->getNextState(4, false));
  }

}
