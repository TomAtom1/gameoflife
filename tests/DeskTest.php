<?php

class DeskTest extends PHPUnit_Framework_TestCase {

  /**
   * @var Cell
   */
  protected $object;

  protected function setUp() {
    $this->object = new \Desk();
  }
  
  /**
   * @test
   */
  public function clearDeskFromDeathCell() {
    $cell = new \Cell(1, 1, true);
    $this->object->addCell($cell);
    $this->assertEquals(1, \count($this->object->getCells()));
    $cell->setNextState(false);
    $cell->switchToNextState();
    $this->object->clearDeskFromDeathCells();
    $this->assertEquals(0, \count($this->object->getCells()));
  }
  
  /**
   * @test
   */
  public function countOfLiveNeigbohrs() {
    $cell = new \Cell(1, 1, true);
    $this->object->addCell($cell);
    $this->assertEquals(1, $this->object->getCountOfLiveCellsInNeigbohr(1, 2));
    $this->assertEquals(1, $this->object->getCountOfLiveCellsInNeigbohr(1, 0));
    $this->assertEquals(0, $this->object->getCountOfLiveCellsInNeigbohr(1, 1));
    $this->assertEquals(1, $this->object->getCountOfLiveCellsInNeigbohr(2, 2));
    $this->assertEquals(0, $this->object->getCountOfLiveCellsInNeigbohr(10, 10));
  }

}
