<?php

class CellTest extends PHPUnit_Framework_TestCase {

  /**
   * @var Cell
   */
  protected $object;

  protected function setUp() {
    $this->object = new Cell(1,1, true);
  }

  /**
   * @test
   */
  public function switchAliveCellToDeath() {
    $this->object->setNextState(false);
    $this->object->switchToNextState();
    $this->assertFalse($this->object->isAlive());
  }

}
