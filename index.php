<?php

require_once __DIR__ . '/vendor/autoload.php';

$desk = new \Desk();

$desk->addCell(new \Cell(3, 3, true));
$desk->addCell(new \Cell(3, 4, true));
$desk->addCell(new \Cell(3, 5, true));
$desk->addCell(new \Cell(5, 5, true));
$desk->addCell(new \Cell(6, 6, true));
$desk->addCell(new \Cell(6, 7, true));
$desk->addCell(new \Cell(7, 7, true));
$desk->addCell(new \Cell(6, 8, true));
$desk->addCell(new \Cell(6, 9, true));
$desk->addCell(new \Cell(2, 9, true));
$desk->addCell(new \Cell(3, 9, true));
$desk->addCell(new \Cell(5, 9, true));
$desk->addCell(new \Cell(9, 9, true));
$desk->addCell(new \Cell(9, 10, true));
$desk->addCell(new \Cell(10, 10, true));
$desk->addCell(new \Cell(13, 14, true));
$desk->addCell(new \Cell(13, 13, true));
$desk->addCell(new \Cell(13, 12, true));

$rules = new \Rules();
$renderer = new \Renderer();
$game = new \Game($desk, $rules, $renderer);
$game->fetchGameStateInOutput();

for ($i = 1; $i < 100; $i++) {
  $game->makeGameStep();
  $game->fetchGameStateInOutput();
}
