<?php

class Desk {

  private $cells = [];

  /**
   * @param int $x
   * @param int $y
   * @return boolean
   */
  public function cellExists($x, $y) {
    if (isset($this->cells[$x][$y]) && \is_object($this->cells[$x][$y])) {
      return true;
    }
    return false;
  }

  public function addCell(\Cell $cell) {
    if ($this->cellExists($cell->getX(), $cell->getY())) {
      throw new \LogicException('tato bunka jiz na hraci plose existuje');
    }
    $this->cells[$cell->getX()][$cell->getY()] = $cell;
  }

  public function getCell($x, $y) {
    if (isset($this->cells[$x][$y])) {
      return $this->cells[$x][$y];
    }
  }

  /**
   * @return \Cell[]
   */
  public function getCells() {
    $cells = [];
    foreach ($this->cells as $rows) {
      foreach ($rows as $cell) {
        $cells[] = $cell;
      }
    }
    return $cells;
  }

  /**
   * @return array
   */
  public function getDeskWithCells() {
    return $this->cells;
  }

  public function clearDeskFromDeathCells() {
    foreach ($this->getCells() as $cell) {
      if (!$cell->isAlive()) {
        unset($this->cells[$cell->getX()][$cell->getY()]);
        unset($cell);
      }
    }
  }

  public function getCountOfLiveCellsInNeigbohr($x, $y) {
    $countOfLiveCells = 0;
    foreach ($this->getNeigbohrsCoordinates($x, $y) as $cordinates) {
      if ($this->cellExists($cordinates[0], $cordinates[1]) && $this->getCell($cordinates[0], $cordinates[1])->isAlive()) {
        $countOfLiveCells++;
      }
    }
    return $countOfLiveCells;
  }

  public function switchCellsToNextsStates() {
    $cells = $this->getCells();
    foreach ($cells as $cell) {
      $cell->switchToNextState();
    }
  }

  public function getNeigbohrsCoordinates($x, $y) {
    $coordinates = [];
    for ($xx = $x - 1; $xx <= $x + 1; $xx++) {
      for ($yy = $y - 1; $yy <= $y + 1; $yy++) {
        if ($xx != $x || $yy != $y) {
          $coordinates[] = [$xx, $yy];
        }
      }
    }
    return $coordinates;
  }

}