<?php

class Rules {

  public function getNextState($countOfAliveNeighbors, $isAlive) {
    return ($isAlive && in_array($countOfAliveNeighbors, [2, 3])) 
            || (!$isAlive && $countOfAliveNeighbors === 3);
  }

}