<?php

class Game {

  /**
   * @var \Renderer
   */
  private $renderer;

  /**
   * @var \Rules
   */
  private $rules;

  /**
   * @var \Desk
   */
  private $desk;

  public function __construct(\Desk $desk, \Rules $rules, \Renderer $renderer) {
    $this->desk = $desk;
    $this->rules = $rules;
    $this->renderer = $renderer;
  }

  public function makeGameStep() {
    $this->setNextStatesForLiveCells();
    $this->setNextStatesForDeathCellsInNeigbohrOfLiveCells();
    $this->desk->switchCellsToNextsStates();
    $this->desk->clearDeskFromDeathCells();
  }

  private function setNextStatesForLiveCells() {
    $cells = $this->desk->getCells();
    foreach ($cells as $cell) {
      $countOfAliveNeighbors = $this->desk->getCountOfLiveCellsInNeigbohr($cell->getX(), $cell->getY());
      $cell->setNextState($this->rules->getNextState($countOfAliveNeighbors, $cell->isAlive()));
    }
  }

  private function setNextStatesForDeathCellsInNeigbohrOfLiveCells() {
    $cells = $this->desk->getCells();
    foreach ($cells as $cell) {
      $x = $cell->getX();
      $y = $cell->getY();
      foreach ($this->desk->getNeigbohrsCoordinates($x, $y) as $coords) {
        if (!$this->desk->cellExists($coords[0], $coords[1])) {
          $countOfAliveNeighbors = $this->desk->getCountOfLiveCellsInNeigbohr($coords[0], $coords[1]);
          $nextStateOfNonExistingCell = $this->rules->getNextState($countOfAliveNeighbors, false);
          if ($nextStateOfNonExistingCell === true) {
            $newLiveCell = new \Cell($coords[0], $coords[1], false);
            $newLiveCell->setNextState($nextStateOfNonExistingCell);
            $this->desk->addCell($newLiveCell);
          }
        }
      }
    }
  }

  public function fetchGameStateInOutput() {
    $this->renderer->renderCells($this->desk->getDeskWithCells());
  }

}