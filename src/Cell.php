<?php

class Cell {

  private $y;
  private $x;
  private $isAlive = false;
  private $nextState = false;

  public function __construct($x, $y, $state) {
    $this->isAlive = (bool) $state;
    $this->nextState = (bool) $state;
    $this->x = (int) $x;
    $this->y = (int) $y;
  }
  
  public function isAlive() {
    return $this->isAlive;
  }

  public function setNextState($nextState) {
    $this->nextState = $nextState;
  }

  public function switchToNextState() {
    $this->isAlive = $this->nextState;
  }
  
  public function getX() {
    return $this->x;
  }
  
  public function getY() {
    return $this->y;
  }

}