<?php

class Renderer {

  public function renderCells(array $cells) {
    echo '<table style="border: 1px solid black; margin:5px;">';
    for ($y = -17; $y < 17; $y++) {
      echo '<tr>';
      for ($x = -14; $x < 22; $x++) {
        if (isset($cells[$x][$y]) && $cells[$x][$y]->isAlive()) {
          $color = "black";
        } else {
          $color = "white";
        }
        echo '<td style="background-color:' . $color . '; width: 2px; height: 1px;"></td>';
      }
      echo '</tr>';
    }
    echo '</table>';
  }

}